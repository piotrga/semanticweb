package dbpedia

import java.net.URI


import dispatch._, Defaults._

import scala.util.Try
import scala.xml.{Node, XML}
import org.joda.time.LocalDate


class DBPedia(){

  def birthPlace(person: URI): Future[Option[String]] =     
    sparql( s"""select ?bp where { <${person.toString}> dbpprop:birthPlace ?bp }""")
      .map(toSimpleResult)
  

  def birthDate(person: URI): Future[Option[LocalDate]] =
    sparql( s"""select ?bd where { <${person.toString}> dbpprop:birthDate ?bd }""")
      .map(toSimpleResult)
      .map(_.map(Dates.toDate))  

  def lookupPerson(name: String) : Future[Seq[URI]] = {
    val lookup = url("http://lookup.dbpedia.org/api/search/KeywordSearch?QueryClass=person").addQueryParameter("QueryString", name)
    for {
      response <- Http(lookup OK as.String)
    } yield XML.loadString(response) \\ "Result" map (result => new URI((result \ "URI").text))
  }

  private def toSimpleResult(node: Node): Option[String] = {
    Try(((node \\ "sparql" \\ "results" head) \\ "result" \\ "binding" \\ "literal").text).toOption
  }

  private def sparql(query: String) : Future[Node] = {
    val httpQuery = url("http://dbpedia.org/sparql")
      .addQueryParameter("default-graph-uri", "http://dbpedia.org")
      .addQueryParameter("format","application/sparql-results+xml")
      .addQueryParameter("timeout","5000")
      .addQueryParameter("query", query)


    Http(httpQuery OK as.String) map XML.loadString
  }

}
