name := "SemanticWeb"

version := "1.0"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  "net.databinder.dispatch" %% "dispatch-core" % "0.11.2",
  "joda-time" % "joda-time" % "2.1",
  "org.joda" % "joda-convert" % "1.2",
  "org.scalatest" % "scalatest_2.10" % "2.1.3" % "test",
  "org.slf4j" % "slf4j-simple" % "1.7.5" % "test"
)