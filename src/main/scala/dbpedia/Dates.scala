package dbpedia

import org.joda.time.format.DateTimeFormat


private[dbpedia] object Dates {
  val pattern = DateTimeFormat.forPattern("yyyy-MM-ddZ")
  def toDate(s: String) = pattern.parseLocalDate(s)
}
