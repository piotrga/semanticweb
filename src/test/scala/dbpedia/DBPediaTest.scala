
package dbpedia

import java.net.URI

import org.scalatest.concurrent._
import org.scalatest.time.{Seconds, Span}
import org.scalatest.{FreeSpec, Matchers}


class DBPediaTest extends FreeSpec with Matchers with ScalaFutures{
  implicit val pc =  new PatienceConfig(timeout = scaled(Span(5,Seconds)))
  val David_Cameron = new URI("http://dbpedia.org/resource/David_Cameron")
  val dBPedia = new DBPedia()

  "Can disambiguate a person" in {
    whenReady(dBPedia.lookupPerson("David Cameron")){
      _.head should be(David_Cameron)
    }
  }

  "Can query the birth place" in {
    whenReady(dBPedia.birthPlace(David_Cameron)){
      _ should be(Some("London, England, U.K."))
    }
  }

  "Can query the birth date" in {
    whenReady(dBPedia.birthDate(David_Cameron)){
      _ should be(Some(Dates.toDate("1966-10-09+02:00")))
    }
  }
}



