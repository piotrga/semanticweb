import dbpedia.DBPedia
import org.joda.time.{LocalDate, Years}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object Question{
  import scala.concurrent.ExecutionContext.Implicits.global

  def ask(query: String)(implicit dbPedia: DBPedia, timeout : Duration) : String = {
    val BirthPlaceOf = "What is the birth place of (.+)".r
    val HowOldIs = "How old is (.+)".r
    val fut = query match{
      case BirthPlaceOf(name) => birthPlaceOf(name)
      case HowOldIs(name) => howOldIs(name)
    }

    Await.result(fut, timeout)
  }

  private def birthPlaceOf(name: String)(implicit dbPedia: DBPedia): Future[String] = {
    for {
      disambiguations <- dbPedia.lookupPerson(name)
      firstDisambiguation = disambiguations.headOption.getOrElse(throw new RuntimeException(s"Can not find '$name'"))
      birthPlace <- dbPedia.birthPlace(firstDisambiguation)
    } yield birthPlace getOrElse "unknown"
  }

  private def howOldIs(name: String)(implicit dbPedia: DBPedia): Future[String] = {
    for {
      disambiguations <- dbPedia.lookupPerson(name)
      firstDisambiguation = disambiguations.headOption.getOrElse(throw new RuntimeException(s"Can not find '$name'"))
      birthDate <- dbPedia.birthDate(firstDisambiguation)
    } yield
      birthDate
        .map(birth => Years.yearsBetween(birth, LocalDate.now()).getYears)
        .map (_.toString)
        .getOrElse("unknown")
  }

}
