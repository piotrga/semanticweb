import dbpedia.DBPedia
import org.scalatest.time.{Seconds, Span}
import org.scalatest.{Matchers, FreeSpec}
import org.scalatest.concurrent.ScalaFutures
import scala.concurrent.duration._

class QuestionTest extends FreeSpec with Matchers{
  implicit val timeout = 10 seconds
  implicit val dbpedia = new DBPedia()

  "Answers to the 'birth place' question" in{
    Question.ask("What is the birth place of David Cameron") should be("London, England, U.K.")
  }

  "Answers to the 'how old' question" in{
    Question.ask("How old is Tony Blair") should be("61")
  }
}
